package com.mcct.sortiesweb.controller;

import javax.naming.Binding;
import javax.validation.Valid;

import com.mcct.sortiesservices.entities.User;
import com.mcct.sortiesservices.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UsersController {
    @Autowired
    private UserService userService;

    @GetMapping({"/users"})
    public String viewUsers(Model model){
        model.addAttribute("users", userService.findAll());
        return "users";
    }

    @GetMapping({"users/create"})
    public String viewCreateUser(){
        return "createuser";
    }

    @GetMapping({"/edit/user/{id}"})
    public String viewUpdateUser(Model model,@PathVariable("id") String id){
        model.addAttribute("userToUpdate", userService.getUserByid(id));
        return "updateuser";
    }

    @PostMapping({"/users/update/{id}"})
    public String updateUser(@PathVariable("id") String id, @Valid User user, BindingResult result, Model model) 
    {
        if(result.hasErrors()) 
        {
            user.setUsername(id);
            return"updateuser";
        }
        userService.editUser(user);

        model.addAttribute("users", userService.findAll());
        return "users";
    }
}
