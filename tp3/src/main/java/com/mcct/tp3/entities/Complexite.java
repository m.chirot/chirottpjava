package com.mcct.tp3.entities;

public enum Complexite {
    TRES_FACILE,
    FACILE,
    MOYENNE,
    DIFICILE,
    TRES_DIFICILE
}
