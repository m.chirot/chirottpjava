package com.mcct.tp3.entities;

import javax.persistence.*;

@Entity
@Table(name = "adresses")
public class Adresse{


    @Override
    public String toString() {
        return "Adresse [complement=" + complement + ", cp=" + cp + ", id=" + id + ", numéro=" + numéro + ", pays="
                + pays + ", rue=" + rue + ", ville=" + ville + "]";
    }

    @Id @Column @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "numero")
    private Integer numéro;

    @Column(name = "rue")
    private String rue;

    @Column(name = "complement")
    private String complement;

    @Column(name = "cp")
    private Integer cp;

    @Column(name = "ville")
    private String ville;

    @Column(name = "pays")
    private String pays;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNuméro() {
        return numéro;
    }

    public void setNuméro(Integer numéro) {
        this.numéro = numéro;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public Integer getCp() {
        return cp;
    }

    public void setCp(Integer cp) {
        this.cp = cp;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

}
