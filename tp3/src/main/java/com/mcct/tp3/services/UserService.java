package com.mcct.tp3.services;

import java.util.ArrayList;
import java.util.List;

import com.mcct.tp3.entities.User;
import com.mcct.tp3.repositories.UserRepository;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class UserService implements UserDetailsService{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    public User createOrUpdate(User user){
        if (StringUtils.isNotEmpty(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        return userRepository.save(user);
    }

    public User getUserByid(String username){
        return userRepository.findById(username).orElse(null);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void delete(User userByid) {
        userRepository.delete(userByid);
    }

    public Page<User> getUserWithPaging(Pageable pageable){
        return userRepository.findAll(pageable);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findById(username).orElse(null);
        if (user != null) {
            List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getRole()));
            return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),authorities);
        }
        throw new UsernameNotFoundException("User '" + username + "' not found or inactive");
    }

    public void setPassword(String userName, String newPassword){
        User user = userRepository.findById(userName).orElse(null);
        String encodedNewPassword = passwordEncoder.encode(newPassword);
        if (user != null) {
            user.setPassword(encodedNewPassword);
            userRepository.save(user);
        }
    }

    public void updatePassword(String newPassword){
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User dbuser = userRepository.findById(user.getUsername()).orElse(null);
        String encodedNewPassword = passwordEncoder.encode(newPassword);
        if (dbuser != null) {
            dbuser.setPassword(encodedNewPassword);
            userRepository.save(dbuser);
        }
    }

}
