package com.mcct.tp3.controllers;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mcct.tp3.dto.ValidationError;

@ControllerAdvice(annotations = RestController.class)
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException ex,
        HttpHeaders headers, HttpStatus status, WebRequest request) 
    {
        ValidationError error = new ValidationError();

        Map<String, String> errorlist = new HashMap<>();

        List<FieldError> errors = ex.getBindingResult().getFieldErrors();
        for (FieldError fieldError : errors ) 
        {
            error.setErrorMessage(fieldError.getDefaultMessage());
            errorlist.put(fieldError.getField(), fieldError.getDefaultMessage());
            error.setFieldErrors(errorlist);

        }
        return new ResponseEntity<>(error, status);
    }
}