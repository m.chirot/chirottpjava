package com.mcct.sortiesrest.controllers;

import java.util.List;

import javax.validation.Valid;

import com.mcct.sortiesservices.entities.*;
import com.mcct.sortiesservices.services.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(path = "/{username}", method = RequestMethod.GET)
    public User get(@PathVariable(name = "username") String username) {
        return userService.getUserByid(username);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public User createOrUpdate(@RequestBody @Valid User user) {
        return userService.createOrUpdate(user);
    }

    @Operation(summary = "Récupétation de tous les utilisateurs")
    @RequestMapping(path = "/_all", method = RequestMethod.GET)
    public List<User> getAllUsers() {
        return userService.findAll();
    }

    @Secured("ADMIN")
    @Operation(summary = "Suppression d'un utilisateur à partit de son identifiant")
    @RequestMapping(path = "/{username}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable(value = "username") String username) {
        userService.delete(userService.getUserByid(username));
    }

    @RequestMapping(method = RequestMethod.GET)
    public Page<User> getUsers(Pageable pageable) {
        return userService.getUserWithPaging(pageable);
    }

    @Secured("ADMIN")
    @Operation(summary = "Mise à jour du mot de passe d'un utilisateur")
    @RequestMapping(path = "/update-password", method = RequestMethod.GET)
    public void setPassword(@RequestParam(value = "username") String userName,
            @RequestParam(value = "password") String newPassword) throws IllegalAccessException {
        userService.setPassword(userName, newPassword);
    }

    @Operation(summary = "Mise à jour du mot de passe d'un utilisateur non admin")
    @RequestMapping(path = "/updatenotadmin-password", method = RequestMethod.GET)
    public void updatePassword(@RequestParam(value = "password") String newPassword) throws IllegalAccessException {
        userService.updatePassword(newPassword);
    }
}
