package com.mcct.sortiesrest;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = "com.mcct")
@EntityScan("com.mcct.sortiesservices.entities")
@EnableJpaRepositories("com.mcct.sortiesservices.repositories")
public class Tp3Application extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(Tp3Application.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder){
		return builder.sources(Tp3Application.class);
	}
}
