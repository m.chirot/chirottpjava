package com.mcct.sortiesservices.entities;

public enum Complexite {
    TRES_FACILE,
    FACILE,
    MOYENNE,
    DIFICILE,
    TRES_DIFICILE
}
