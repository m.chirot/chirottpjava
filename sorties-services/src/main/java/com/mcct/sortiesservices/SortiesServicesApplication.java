package com.mcct.sortiesservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SortiesServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SortiesServicesApplication.class, args);
	}

}
